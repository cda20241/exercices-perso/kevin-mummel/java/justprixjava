package org.example;

import java.io.BufferedReader;// Lire du texte à partir d'un fichier
import java.io.BufferedWriter;// Ecrire du texte dans un fichier
import java.io.FileWriter;// Créer un fichier ou écrire dans un fichier existant
import java.io.FileReader;// Lire un fichier
import java.io.IOException;// Gérer les erreurs liées aux opérations d'entrée/sortie
import java.util.ArrayList;// Créer et manipuler des listes dynamiques
import java.util.List; // Déclarer des listes
import java.util.Random;// Générer des nombres aléatoires
import java.util.Scanner;// Lire l'entrée de l'utilisateur depuis la console

public class Main {
    private static final List<String> matrix = new ArrayList<>(); // Créer une variable "matrix" qui est une liste de chaine de caractères
    private static final Scanner scanner = new Scanner(System.in); // Créer une variable "scanner" qui est un objet Scanner qui va lire l'entrée utilisateur depuis la console (System.in)


    /**
     * Boucle principale qui affiche un menu, gère les entrées de l'utilisateur
     * et appelle les autres méthodes en fonction du choix de l'utilisateur.
     * @param args
     */
    public static void main(String[] args) {
        while (true) {
            displayMenu();
            String choice = scanner.nextLine();

            switch (choice) {
                case "1":
                    System.out.print("Quel est votre nom ? ");
                    String userName = scanner.nextLine();
                    playGame(userName);
                    break;
                case "2":
                    displayLast10Results("Resultats_fichier.txt");
                    break;
                case "3":
                    displayAllResults("Resultats_fichier.txt");
                    scanner.close(); // Déplacez cette ligne ici
                    return;
                default:
                    System.out.println("Option invalide. Veuillez choisir une option valide.");
                    break;
            }
        }
    }

    /**
     * affiche un menu à l'utilisateur
     */
    private static void displayMenu() { // Méthode privé "displayMenu" qui ne renvoie rien (void) et sans argument
        System.out.println("Menu :"); // Afficher Les choix du menu
        System.out.println("1. Jouer au jeu");
        System.out.println("2. Afficher les 10 derniers résultats");
        System.out.println("3. Afficher tous les résultats et quitter");
    }

    /**
     * gère le jeu principal
     * @param userName
     */
    private static void playGame(String userName) { // Méthode privé "playGame" qui ne renvoie rien et prend en argument le nom d'utilisateur
        long startTime = System.currentTimeMillis(); // Créer la variable startTime de type long pour stocker le temps de départ du jeu
        Random random = new Random(); // Créer un objet random pour générer des nombres aléatoires
        int justPrice = random.nextInt(1000) + 1; // Créer un nombre aléatoire de 1 à 1000 et le stocker dans la variable "random"
        boolean running = true; // Créer une variable booléenne par défaut en vrai
        int userPrice; // Initialiser la variable "userPrice" à 0

        do { // Boucle qui exectute toute les instructions tant que c'est vrai
            System.out.print("Entrez un prix entre 0 et 1000: "); // Afficher "Entrez un prix"
            userPrice = Integer.parseInt(scanner.nextLine()); // Lire l'entrée utilisateur et convertir en nombre et sotcker dans la variable userprice

            if (userPrice == justPrice) { // Si la variable "userPrice" = "justPrice"
                System.out.println("Trouvé !"); // Alors Afficher "Trouvé"
                running = false; // La variable "running" est fausse donc la boucle est arrêtée

            } else if (userPrice > justPrice) { // sinon si la variable userPrice est plus grande que "justPrix"
                System.out.println("C'est moins"); // Alors on affiche "c'est moins"

            } else { // Sinon
                System.out.println("C'est plus"); // on affiche C'est plus
            }
        } while (running); //La boucle est fini si "running" est fausse ou se répète si vrai


        long endTime = System.currentTimeMillis(); // Enregistrer dans la variable "endTime" le temps à la fin du jeu
        int duration = (int) ((endTime - startTime) / 1000); // Calculer la durée et l'enrregistrer dans la variable "duration"
        String result = String.format("Le joueur %s a trouvé %d en %d secondes", userName, userPrice, duration); // Créer la variable result avec ce format ...
        matrix.add(result); // Ajouter la variable "result" à la liste matrix
        writeMatrixToFile("Resultats_fichier.txt", matrix); // Appeler la méthode "writeMatrixToFile" pour écrire la liste matrix dans le fichier.txt
    }

    /**
     * Affiche les 10 derniers résultats
     * @param fileName
     */
    private static void displayLast10Results(String fileName) { // Créer une méthode "displayLast10Results" qui prend en argument le nom du fichier.txt

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) { // gérer automatiquement la ressource BufferedReader, ouvrir et lire le fichier
            List<String> lines = new ArrayList<>(); // Créer une liste "lines"
            String line; // Déclarer la variable "line"

            while ((line = reader.readLine()) != null) { // Créer une boucle qui lit chaque ligne du fichier à l'aide de la méthode readLine() tant que line n'est pas null
                lines.add(line); // Rajouter la ligne lu à chaque boucle
            }
            int start = Math.max(0, lines.size() - 10); // compter le nombre de ligne et soustraire 10, la fonction "Math.max" renvoie la plus grande valeur pour éviter de commencé la liste en dessous de la ligne numéro 0
            for (int i = start; i < lines.size(); i++) { // Créer une boucle qui initialise i à la valeur de start jusqu'à ce que i < au nombre de ligne
                System.out.println(lines.get(i)); // Afficher la ligne à chaque itération
            }
        } catch (IOException e) { // Gérer les exceptions
            e.printStackTrace(); // Affiche les exceptions
        }
    }

    /**
     * Affiche tout les résultats
     * @param fileName
     */
    private static void displayAllResults(String fileName) { // Créer une méthode "displayAllResults" qui prend en argument le nom du fichier.txt

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) { // gérer automatiquement la ressource BufferedReader, ouvrir et lire le fichier
            String line; // Déclarer la variable "line"

            while ((line = reader.readLine()) != null) { // Créer une boucle qui lit chaque ligne du fichier à l'aide de la méthode readLine() tant que line n'est pas null
                System.out.println(line); // Afficher la liste "line"
            }
        } catch (IOException e) { // Gérer les exceptions
            e.printStackTrace(); // Affiche les exceptions
        }
    }

    /**
     * Enregistre les résultats dans un fichier .txt
     * @param fileName
     * @param matrix
     */
    private static void writeMatrixToFile(String fileName, List<String> matrix) { // Créer une méthode avec 2 arguments, le nom du fichier et la liste matrix

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) { // gérer automatiquement la ressource BufferedReader, ouvrir et écrire le fichier (FileWriter à true signifie ajoute du texte et ne supprime pas le texte deja présent)

            for (String line : matrix) { // créer une Boucle qui prend chaque ligne de la liste "matrix"
                writer.write(line + "\n"); // Ecrire les ligne avec la méthode "write" et sauter des lignes (+ "\n")
            }
        } catch (IOException e) { // Gérer les exceptions
            e.printStackTrace(); // Afficher les exceptions
        }
    }
}
